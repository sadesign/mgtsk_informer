$(document).ready(function () {
    function readProperty(property, defValue) {
        if (localStorage[property] == null) {
            return defValue;
        }

        return localStorage[property];
    }
    if (localStorage["mgtskKey"] && localStorage["mgtskKey"] !== '') {
        $('#localstorage-save').addClass('hidden');
        $('.js-haskey').removeClass('hidden');
    }
    $(".js-key").each(function (i, el) {
        if(el.type == 'checkbox') {
            if(readProperty(el.name, '') == 'true') {
                el.checked = true;
            }
        } else {
            el.value = readProperty(el.name, '');
        }
    });

    $('.js-haskey-change').on('click', function () {
        $('#localstorage-save').removeClass('hidden');
        $('.js-haskey').addClass('hidden');
    });
    $('form').submit(function () {
        $(this).find('input').each(function (i, el) {
            if (el.type == 'text') {
                localStorage[el.name] = el.value;
            } else if (el.type == 'checkbox') {
                localStorage[el.name] = el.checked;
            }

        });
//        var arr = $(this).serializeArray();
//        arr.forEach(function (el, i) {
//            localStorage[el.name] = el.value;
//        });
//    return false;
    });
});