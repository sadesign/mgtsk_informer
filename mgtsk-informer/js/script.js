$(document).ready(function () {
    target = 'http://acceptor.mgtsk.ru/';
    key = '?key=' + localStorage['mgtskKey'];
    $.ajax({
        url: target + 'getNotifications' + key,
        type: 'POST',
        beforeSend:
                function () {
                    $('.mgt').removeClass('mgt-drag-hidden');
                },
        success:
                function (response) {
                    if (response.data !== null) {
                        $('.mgt').addClass('mgt-drag-hidden');
                        $('.informar-wrapper').empty();
                        console.log(response);
                        var iconText = '';
                        var body = '';
                        var name = '';
                        if (response.status.code === 'ok') {
                            $('.informer-wrapper').append('<ul class="informer-list"></ul>');
                            $(response.data).each(function (i, el) {
                                console.log(el);
                                var type = el.Subject.Type;
                                switch (type) {
                                    case 'comment':
                                        iconText = 'Комментарий';
                                        body = el.Content.Text;
                                        name = el.Name + ' ' + el.Content.Subject.Name;
                                        break;
                                    case 'task':
                                        iconText = 'Задача';
                                        body = el.Subject.Name;
                                        name = el.Name;
                                        break;
                                    case 'event':
                                        iconText = 'Событие';
                                        body = el.Subject.Name;
                                        name = el.Name;
                                        break;
                                }
                                link = '';
                                var link = 'https://' + response.info.adress + '/' + el.Subject.Type + '/' + el.Subject.Id + '/card';
                                $('.informer-list').append('<li/>');
                                $('.informer-list li:last').addClass('informer-list-item');
                                $('.informer-list li:last').addClass(type);
                                $('.informer-list li:last').attr('data-id', el.Id);

                                $('.informer-list li:last').append('<div class="informer-list-item-icon ' + type + '"/>');
//                                $('.informer-list li:last .informer-list-item-icon').append('<div class="informer-list-item-icon-img informer-list-item-icon-img_' + type + '"/>');
                                $('.informer-list li:last .informer-list-item-icon').append('<div class="informer-list-item-icon-text"/>');
                                $('.informer-list li:last .informer-list-item-icon .informer-list-item-icon-text').text(iconText);

                                $('.informer-list li:last').append('<div class="informer-list-item-cont"/>');
                                $('.informer-list li:last .informer-list-item-cont').append('<a class="informer-list-title"/>');
                                $('.informer-list li:last .informer-list-item-cont .informer-list-title').attr('href', link);
                                $('.informer-list li:last .informer-list-item-cont .informer-list-title').attr('target', '_blank');
                                $('.informer-list li:last .informer-list-item-cont .informer-list-title').text(name);
                                $('.informer-list li:last .informer-list-item-cont').append('<div class="informer-list-text"/>');
                                $('.informer-list li:last .informer-list-item-cont .informer-list-text').text(body);
                                $('.informer-list li:last .informer-list-item-cont').append('<div class="informer-list-date"/>');
                                $('.informer-list li:last .informer-list-item-cont .informer-list-date').text(el.TimeCreated);
                            });
                        } else {
                            $('.informer-wrapper').empty();
                            $('.informer-wrapper').append('<div class="informer-error" />');
                            $('.informer-wrapper .informer-error').text('У вас нет новых сообщений.');
                            console.log('Ясень не ответил мне...');
                        }
                    } else {
                        $('.informer-wrapper').empty();
                        console.log('error');
                        $('.informer-wrapper').append('<div class="informer-error" />');
                        $('.informer-wrapper .informer-error').html('Вы что-то делаете не так. Загляните в \<a id="go-to-options" href="options.html"\ target=\"blank\">настройки\<\/a\>.');
                        $('.mgt').addClass('mgt-drag-hidden');
                    }
                },
        error:
                function (data) {
                    console.log(data);
                }
    });
});
