if ($('.files-list .files-list-block li').length > 0) {
    chrome.extension.sendRequest({key: 'key'}, function (response) {
        var count = $('.files-list-item:not(.external-file-link) .j-file-link.j-file-name').length;
        target = 'https://mgtsk.ru/acceptor/';
        key = response.key;
        $.ajax({
            url: target + 'getInfo',
            type: 'POST',
            data: {key: key},
            success: function (response) {
                var re = /staff\/(\d*)\//;
                var str = $('.user-name-wrapper').attr('href');
                var m;
                if ((m = re.exec(str)) !== null && m[m.index] == response.info.employee_id) {
                    if ($('.files-list-item:not(.external-file-link) .j-file-link.j-file-name').length > 1) {
                        if (response.info.adress == document.location.host) {
                            console.log('Hi, I\'m MGTSK informer!');
                            $('.files-list .files-list-block').append('<a href="#" class="mgtsk-download" data-load="Подождите немного, сейчас мы отдадим вам архив">Скачать все файлы</a>');

                            $('.mgtsk-download').attr('data-content', '(' + count + ')');
                        }
                    }
                }
            }
        });
        $(document).on('click', '.mgtsk-download', function (e) {
            e.preventDefault();
            changeText($(this));
            var getBinaryArray = [];
            var getBinaryArrayRes = [];
            var zip = new JSZip();
            $('.files-list-item:not(.external-file-link) .j-file-link.j-file-name').each(
                    function (i, el) {
                        getBinaryArray[i] = $.ajax({
                            url: window.location.origin + $(el).attr('href'),
                            type: 'GET',
                            dataType: 'binary',
                            responseType: 'arraybuffer',
                            processData: false,
                            beforeSend: function (xhr) {
                                getBinaryArrayRes[i] = {};
                                getBinaryArrayRes[i].name = $(el).text();
                            },
                            success: function (result) {
                                var arrayBufferView = new Uint8Array(result);
                                base64 = base64ArrayBuffer(arrayBufferView);
                                getBinaryArrayRes[i].content = base64;
                                zip.file(getBinaryArrayRes[i].name, getBinaryArrayRes[i].content, {base64: true});
                                $('.mgtsk-download').attr('data-content', '(' + Object.keys(zip.files).length + '/' + count + ')');
                            }
                        });
                    }
            )
            $.when
                    .apply($, getBinaryArray)
                    .then(function () {
                        var content = zip.generate({type: "blob"});
                        saveAs(content, new Date().getTime() + ".zip");
                    })
                    .then(function () {
                        changeText($('.mgtsk-download'));
                        $('.mgtsk-download').attr('data-content', '(' + count + ')');
                    });
        });

    }
    );
}

function base64ArrayBuffer(arrayBuffer) {
    var base64 = ''
    var encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

    var bytes = new Uint8Array(arrayBuffer)
    var byteLength = bytes.byteLength
    var byteRemainder = byteLength % 3
    var mainLength = byteLength - byteRemainder

    var a, b, c, d
    var chunk

    // Main loop deals with bytes in chunks of 3
    for (var i = 0; i < mainLength; i = i + 3) {
        // Combine the three bytes into a single integer
        chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2]

        // Use bitmasks to extract 6-bit segments from the triplet
        a = (chunk & 16515072) >> 18 // 16515072 = (2^6 - 1) << 18
        b = (chunk & 258048) >> 12 // 258048   = (2^6 - 1) << 12
        c = (chunk & 4032) >> 6 // 4032     = (2^6 - 1) << 6
        d = chunk & 63               // 63       = 2^6 - 1

        // Convert the raw binary segments to the appropriate ASCII encoding
        base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d]
    }

    // Deal with the remaining bytes and padding
    if (byteRemainder == 1) {
        chunk = bytes[mainLength]

        a = (chunk & 252) >> 2 // 252 = (2^6 - 1) << 2

        // Set the 4 least significant bits to zero
        b = (chunk & 3) << 4 // 3   = 2^2 - 1

        base64 += encodings[a] + encodings[b] + '=='
    } else if (byteRemainder == 2) {
        chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1]

        a = (chunk & 64512) >> 10 // 64512 = (2^6 - 1) << 10
        b = (chunk & 1008) >> 4 // 1008  = (2^6 - 1) << 4

        // Set the 2 least significant bits to zero
        c = (chunk & 15) << 2 // 15    = 2^4 - 1

        base64 += encodings[a] + encodings[b] + encodings[c] + '='
    }

    return base64
}

function changeText(el) {
    var textOld = el.text();
    var textNew = el.attr('data-load');
    el.text(textNew);
    el.attr('data-load', textOld);
}