function install_notice() {
    if (localStorage.getItem('mgtskKey'))
        return;
    if (chrome.runtime.openOptionsPage !== undefined) {
        chrome.runtime.openOptionsPage();
    } else {
        chrome.tabs.create({'url': 'chrome-extension://' + chrome.runtime.id + '/html/options.html'});
    }
}
install_notice();

// Background Page
chrome.extension.onRequest.addListener(function (request, sender, sendResponse) {
    if (request.key) {
        key = localStorage['mgtskKey'];
        sendResponse({key: key});
    } else {
        sendResponse({});
    }
});

localStorage.ids = '';

function show() {
    target = 'http://acceptor.mgtsk.ru/';
    key = '?key=' + localStorage['mgtskKey'];
    $.ajax({
        url: target + 'getNotifications' + key,
        type: 'POST',
        beforeSend: function () {

        },
        success: function (response) {
//                    console.log(response);
            if (response.status.code === 'ok') {
                body = '';
                if (localStorage.ids != '') {
                    ids = JSON.parse(localStorage.ids);
                } else {
                    ids = [];
                }
                idsA = [];
                $(response.data).each(function (i, el) {
                    idsA[i] = el.Id;
                    if (ids.indexOf(el.Id) === -1) {
                        if (el.Subject.Type === 'comment') {
                            var body = el.Content.Text;
                            var name = el.Name + ' ' + el.Content.Subject.Name;
                        }
                        if (el.Subject.Type === 'task') {
                            var body = el.Subject.Name;
                            var name = el.Name;
                        }
                        if (el.Subject.Type === 'event') {
                            var body = el.Subject.Name;
                            var name = el.Name;
                        }
                        if (el.Subject.Type === 'project') {
                            var body = el.Content;
                            var name = el.Name;
                        }
                        var link = 'https://' + response.info.adress + '/' + el.Subject.Type + '/' + el.Subject.Id + '/card';
                        var dfd = $.Deferred();
                        var promise = dfd.promise();
                        if (el.Subject.Type === 'comment' || (el.Subject.Type === 'event' && el.Subject.Owner !== null)) {
                            if (el.Subject.Type === 'event') {
                                var tempId = parseInt(el.Subject.Owner.Id);
                            }
                            if (el.Subject.Type === 'comment') {
                                var tempId = parseInt(el.Content.Author.Id);
                            }
                            $.ajax({
                                url: target + 'getEmploee',
                                type: 'POST',
                                data: {
                                    key: localStorage['mgtskKey'],
                                    data: {
                                        Id: tempId
                                    }
                                },
                                beforeSend: function () {

                                },
                                success: function (result) {
                                    var icon = 'https://' + result.data.Photo;
                                    dfd.resolve(name, icon, body, link);
                                },
                                error: function () {
                                    var icon = 'img/icos_80.png';
                                    dfd.resolve(name, icon, body, link);
                                }
                            });
                        } else {
                            var icon = 'img/icos_80.png';
                            dfd.resolve(name, icon, body, link);
                        }
//                                return dfd.promise();

                        promise.done(function (name, icon, body, link) {
                            var notification = new Notification(name, {
                                icon: icon,
                                body: body,
                            });
                            notification.onclick = function () {
                                window.open(link);
                            };
                            var yourSound = new Audio('audio/sms-a2.mp3');
                            yourSound.play();
                        });
                    }
                });
                localStorage.ids = JSON.stringify(idsA);
                if (idsA.length == 0) {
                    t = '';
                } else {
                    t = '' + idsA.length;
//                            t = ' ';
                }
                chrome.browserAction.setBadgeText({text: t});
            } else {
                chrome.browserAction.setBadgeText({text: ''});
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}
function setReadAll() {
    target = 'http://acceptor.mgtsk.ru/';
    key = '?key=' + localStorage['mgtskKey'];
    $.ajax({
        url: target + 'getNotifications' + key,
        type: 'POST',
        beforeSend: function () {

        },
        success: function (response) {
            console.log(response);
            if (response.status.code === 'ok') {
                body = '';
                if (localStorage.ids != '') {
                    ids = JSON.parse(localStorage.ids);
                } else {
                    ids = [];
                }
                idsA = [];
                $(response.data).each(function (i, el) {
                    idsA[i] = el.Id;
                });
                localStorage.ids = JSON.stringify(idsA);
                if (idsA.length == 0) {
                    t = '';
                } else {
                    t = '' + idsA.length;
//                            t = ' ';
                }
                chrome.browserAction.setBadgeText({text: t});
            } else {
                chrome.browserAction.setBadgeText({text: ''});
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}
// Conditionally initialize the options.
if (localStorage.mgtskKey) {
    if (!localStorage.isInitialized) {
        localStorage.isActivated = true;   // The display activation.
        localStorage.frequency = 1;        // The display frequency, in minutes.
        localStorage.isInitialized = true; // The option initialization.
    }
}

// Test for notification support.
if (window.Notification) {
    // While activated, show notifications at the display frequency.
    if (JSON.parse(localStorage.isActivated)) {
        if (localStorage.setReadAll == 'true') {
            setReadAll();
        } else {
            show();
        }
    }
    var interval = 0; // The display interval, in minutes.
    setInterval(function () {
        interval++;
        if (
            JSON.parse(localStorage.isActivated) &&
            localStorage.frequency <= interval
        ) {
            show();
            interval = 0;
        }
    }, 30000);
}
